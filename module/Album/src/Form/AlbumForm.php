<?php

namespace Album\Form;

// use DomainException;
use Laminas\Form\Form;


class AlbumForm extends Form
{
  /**
   * Undocumented variable
   *
   * @var [type]
   */
  public $id;

  /**
   * Undocumented variable
   *
   * @var [type]
   */
  public $artist;

  /**
   * Undocumented variable
   *
   * @var [type]
   */
  public $title;

  // Add this property:
  private $inputFilter;

  /**
   * construct function
   *
   * @param [type] $name
   */
  public function __construct($name = null)
  {
    // We will ignore the name provided to the constructor
    parent::__construct('album');

    $this->add([
      'name' => 'id',
      'type' => 'hidden',
    ]);

    $this->add([
      'name'    => 'title',
      'type'    => 'text',
      'options' => [
        'label' => 'Title',
      ],
    ]);

    $this->add([
      'name'    => 'artist',
      'type'    => 'text',
      'options' => [
        'label' => 'Artist',
      ],
    ]);

    $this->add([
      'name'       => 'submit',
      'type'       => 'submit',
      'attributes' => [
        'value' => 'Go',
        'id'    => 'submitbutton',
      ],
    ]);
  }

}
